import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { IConfiguration } from './configuration';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _apiUrl: string;
  get apiUrl(): string{
    return this._apiUrl;
  }
  set apiUrl(value: string){
    this._apiUrl = '';
    this._apiUrl = value;
  }

  private _fullApiEndPoint: string;
  get fullApiEndPoint(): string{
    return this._fullApiEndPoint;
  }
  set fullApiEndPoint(value: string){
    this._fullApiEndPoint = '';
    this._fullApiEndPoint = value;
  }

  private childMenus: number[] = [];

  constructor(private http: HttpClient) {
    this.apiUrl = 'https://jsonplaceholder.typicode.com';
  }

  getToken(): Observable<any> {
    return this.http.post<any>('http://dev.api.smart.asd-tech.mx:10264/api/Auth/RequestToken', {
      "Username": "luis.puc@asd-tech.mx",
      "Password": "5s$8rv_W",
      "Uuid": "8FBDA40E-ACD8-E911-8154-D4AE52CF99E8",
      "Key" : "8FBDA40E-ACD8-E911-8154-D4AE52CF99E8"
    });
  }

  getConfiguration(token: string): Observable<IConfiguration[]> {
    return this.http.get<IConfiguration[]>('http://dev.api.smart.asd-tech.mx:10264/api/Configuration', {
      headers: {
        'Authorization': 'bearer ' + token
      }
    }).pipe(/*tap(data => console.log('Configuration: ' + JSON.stringify(data))),*/
      map(menus =>{
        let filterMenus: IConfiguration[] = [];

        for( let i=0; (i<menus.length&&!this.childMenus.includes(i)) ;i++ ){
          let menu = menus[i];
          menu.subMenu = this.getChildrenMenus(menus, menu);
          filterMenus.push(menu);
        }
        
        return filterMenus;
      })
    );
  }

  private getChildrenMenus(menus: IConfiguration[], menu: IConfiguration): IConfiguration[] {
    let childrenMenus: IConfiguration[] = [];
    
    for(let j in menus){
      let menuChild = menus[j];
      if( (menuChild.menuId != menu.menuId) && (menuChild.menuParentId == menu.menuId) ){
        this.childMenus.push(parseInt(j));
        menuChild.subMenu = this.getChildrenMenus(menus, menuChild);
        childrenMenus.push(menuChild);
      }
    }
    
    return childrenMenus;
  }

  private handleError(err: HttpErrorResponse){
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = `An error occurred: ${err.error.message}`;
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
