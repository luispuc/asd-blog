import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUser } from './user';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersEndPoint = '/users';

  constructor(private apiServiceUser: ApiService, private http: HttpClient) {
    
  }

  getUsers(): Observable<IUser[]>{
    this.setFullEndPoint();

    return this.http.get<IUser[]>(this.apiServiceUser.fullApiEndPoint)
      .pipe(
        //tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  getUser(id: number): Observable<IUser | undefined>{
    return this.getUsers().pipe(
      map((users: IUser[]) => users.find(u => u.id === id))
    );
  }

  private handleError(err: HttpErrorResponse){
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = `An error occurred: ${err.error.message}`;
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  private setFullEndPoint(): void {
    this.apiServiceUser.fullApiEndPoint = this.apiServiceUser.apiUrl + this.usersEndPoint;
    
  }
}
