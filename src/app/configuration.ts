export interface IConfiguration {
    name: string;
    moduleId: number;
    menuParentId: number;
    permissionId: number;
    path: string;
    description: string;
    step: number;
    isInactive: boolean;
    menuId: number;
    subMenu: IConfiguration[];
}
