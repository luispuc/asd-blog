import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'asd-catalogues-list',
  templateUrl: './catalogues-list.component.html',
  styleUrls: ['./catalogues-list.component.css']
})
export class CataloguesListComponent implements OnInit {

  titleCatalogue: string = 'Catalogues';
  toAssign: Array<any> = [];
  assigned: Array<any> = []

  constructor() { }

  ngOnInit() {
    this.toAssign = [
      {
        title: 'Asignar permisos',
        module: 'Permisos',
        app: 'General',
        id: 12
      },
      {
        title: 'Eliminar permisos',
        module: 'Permisos',
        app: 'General',
        id: 123
      },
      {
        title: 'Editar permisos',
        module: 'Permisos',
        app: 'General',
        id: 1234
      }
    ]
  }

}
