import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'asd-dual-list',
  templateUrl: './dual-list.component.html',
  styleUrls: ['./dual-list.component.css']
})
export class DualListComponent implements OnInit {

  @Input()toAssign: Array<any> = [];
  toAssignFiltered: Array<any> = [];
  @Input()assigned: Array<any> = [];
  assignedFiltered: Array<any> = [];

  constructor() { }

  _filterToAssign: string;
  get filterToAssign(): string{
    return this._filterToAssign;
  }
  set filterToAssign(value: string) {
    this._filterToAssign = value;
    this.toAssignFiltered = this._filterToAssign ? this.performFilterAssign(this.filterToAssign) : this.toAssign;
  }

  performFilterAssign(filterBy: string): Array<any>{
    filterBy = filterBy.toLocaleLowerCase();
    return this.toAssign.filter(el => {
      let filter = false;
      for(let key in el){
        let valueMatch = el[key].toString();

        if(valueMatch.toLocaleLowerCase().indexOf(filterBy) !== -1){
          filter = true;
          break;
        }
      }

      return filter;
    });
  }

  _filterAssigned: string;
  get filterAssigned(): string {
    return this._filterAssigned;
  }
  set filterAssigned(value: string) {
    this._filterAssigned = value;
    this.assignedFiltered = this._filterAssigned ? this.performFilterAssigned(this.filterAssigned) : this.assigned
  }

  performFilterAssigned(filterBy: string): Array<any> {
    filterBy = filterBy.toLocaleLowerCase();
    return this.assigned.filter(el => {
      let filter = false;
      for(let key in el){
        let valueMatch = el[key].toString();

        if(valueMatch.toLocaleLowerCase().indexOf(filterBy) !== -1){
          filter = true;
          break;
        }
      }

      return filter;
    });
  }

  ngOnInit() {
    this.toAssignFiltered = this.toAssign;
    this.assignedFiltered = this.assigned;
  }

  assignAll(): void {
    this.assignedFiltered = this.assignedFiltered.concat(this.toAssignFiltered);
    this.toAssignFiltered = [];
  }

  removeAll(): void {
    this.toAssignFiltered = this.toAssignFiltered.concat(this.assignedFiltered);
    this.assignedFiltered = []
  }

  assign(valueItem: number): void {
    let valueItemToAssign: number = valueItem;
    if(valueItemToAssign){
      let itemToAssign: object = this.toAssignFiltered.find(el => el.id === valueItemToAssign);
      let indexItemToAssign: number = this.toAssignFiltered.findIndex(el => el.id === valueItemToAssign);

      this.assignedFiltered.push(itemToAssign);
      this.toAssignFiltered.splice(indexItemToAssign, 1);
    }
  }

  removeAssigned(valueItem: number): void {
    let valueItemRemove: number = valueItem;
    if(valueItemRemove){
      let itemToRemove: object = this.assignedFiltered.find(el => el.id == valueItemRemove);
      let indexItemToRemove: number = this.assignedFiltered.findIndex(el => el.id == valueItemRemove);

      this.toAssignFiltered.push(itemToRemove);
      this.assignedFiltered.splice(indexItemToRemove, 1);
    }
  }

}
