import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DualListComponent } from './dual-list.component';
import { Routes, RouterModule } from '@angular/router';
import { CataloguesListComponent } from './catalogues-list.component';

const routes: Routes = [
  {path: 'catalogues', component: CataloguesListComponent}
]

@NgModule({
  declarations: [DualListComponent, CataloguesListComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class CatalogueModule { }
