import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { IAlbum } from './album';
import { catchError, map } from 'rxjs/operators';
import { IPhoto } from '../photos/photo';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {
  private albumsEndPoint: string = '/albums';

  constructor(
    private apiServiceAlbums: ApiService,
    private http: HttpClient
  ) { }

  getAlbums(): Observable<IAlbum[]> {
    this.setFullEndPoint();

    return this.http.get<IAlbum[]>(this.apiServiceAlbums.fullApiEndPoint)
      .pipe(
        catchError(this.handleError)
      );
  }

  getAlbum(id: number): Observable<IAlbum | undefined> {
    return this.getAlbums().pipe(
      map(albums => albums.find(a => a.id = id))
    );
  }

  getAlbumPhotos(id: number): Observable<IPhoto[]> {
    let completeEndPoint = '/' + id.toString() + '/photos';
    this.setFullEndPoint(completeEndPoint);

    return this.http.get<IPhoto[]>(this.apiServiceAlbums.fullApiEndPoint)
      .pipe(
        catchError(this.handleError)
      );
  }
  
  private handleError(err: HttpErrorResponse){
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = `An error occurred: ${err.error.message}`;
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  
  private setFullEndPoint(completeUrl: string = ''): void {
    this.apiServiceAlbums.fullApiEndPoint = this.apiServiceAlbums.apiUrl + 
      this.albumsEndPoint +
      completeUrl;
  }
}
