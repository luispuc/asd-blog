import { Component, OnInit } from '@angular/core';
import { AlbumService } from './album.service';
import { IAlbum } from './album';
import { PhotoService } from '../photos/photo.service';
import { IPhoto } from '../photos/photo';

@Component({
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  titleAlbum: string = 'Albums';
  albums: IAlbum[];
  photos: IPhoto[];

  constructor(private albumService: AlbumService, private photoService: PhotoService) { }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.photoService.getPhotos().subscribe({
      next: photos => {
        this.photos = photos;
        
        this.getAlbums();
      }
    });
  }

  getAlbums() {
    this.albumService.getAlbums().subscribe({
      next: albums => {
        albums.forEach((album, i) => {
          album.imgThumbnailUrl = this.photos
            .find(p => p.albumId == album.id).thumbnailUrl;
          albums[i] = album;
        });
        this.albums = albums;
      }
    });
  }

}
