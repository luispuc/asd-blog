import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IAlbum } from './album';
import { AlbumService } from './album.service';
import { IPhoto } from '../photos/photo';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryComponent } from 'ngx-gallery';

@Component({
  templateUrl: './album-photos.component.html',
  styleUrls: ['./album-photos.component.css']
})
export class AlbumPhotosComponent implements OnInit {
  album: IAlbum = {userId:0, id: 0, title: '', imgThumbnailUrl: ''};
  photos: IPhoto[];
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];
  @ViewChild('gallery', {static: true}) gallery: NgxGalleryComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private albumService: AlbumService
  ) { }

  ngOnInit() {
    const idAlbum = +this.route.snapshot.paramMap.get('id');
    this.albumService.getAlbum(idAlbum).subscribe({
      next: album => {
        this.album = album;

        this.getAlbumPhotos(idAlbum);
      }
    });

    this.galleryOptions = [
      { "image": false, "thumbnails": false, "width": "0px", "height": "0px" },
      { "breakpoint": 500, "width": "300px", "height": "300px", "thumbnailsColumns": 3 },
      { "breakpoint": 300, "width": "100%", "height": "200px", "thumbnailsColumns": 2 }
    ];
  }

  getAlbumPhotos(id: number) {
    this.albumService.getAlbumPhotos(id).subscribe({
      next: photos => {
        this.photos = photos;

        this.photos.forEach(photo => {
          let galleryImage: NgxGalleryImage = {
            small: photo.thumbnailUrl,
            medium: photo.url,
            big: photo.url
          };

          this.galleryImages.push(galleryImage);
          
        });
      }
    });
  }

  onBack() {
    this.router.navigate(['/albums']);
  }

  openPreview(index: number) {
    this.gallery.openPreview(index);
  }

}
