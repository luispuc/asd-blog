import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AlbumComponent } from './album.component';
import { Routes, RouterModule } from '@angular/router';
import { AlbumPhotosComponent } from './album-photos.component';

const routes: Routes = [
  {path: 'albums', component: AlbumComponent},
  {path: 'albums/:id', component: AlbumPhotosComponent}
]

@NgModule({
  declarations: [
    AlbumComponent,
    AlbumPhotosComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
})
export class AlbumModule { }
