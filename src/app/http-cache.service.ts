import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpCacheService {

  private requests: {[key: string]: HttpResponse<any>} = {};

  constructor() { }
  //When handle a request the response asign to local object variable "request"
  //with the url from the request like key
  put(url: string, response: HttpResponse<any>): void {
    /*let requests: {[key: string]: HttpResponse<any>} = JSON.parse(
      localStorage.getItem('asd_requests_get')
    ) || [];*/

    //this.requests[url] = response;
    localStorage.setItem(url, JSON.stringify(response));
    //localStorage.setItem('asd_requests_get', JSON.stringify(requests));
  }
  //Retrieve the data cached from request by url
  get(url: string): HttpResponse<any> | undefined {
    /*let requests: {[key: string]: HttpResponse<any>} = JSON.parse(
      localStorage.getItem('asd_requests_get')
    ) || [];*/
    console.log('Are requests: ', this.requests)
    //return this.requests ? this.requests[url] : undefined;
    return JSON.parse(localStorage.getItem(url));
  }

  invalidateCache(): void {
    //localStorage.removeItem('asd_requests_get');
    this.requests = {};
  }
}
