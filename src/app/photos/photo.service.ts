import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { IPhoto } from './photo';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  private photosEndPoint: string = '/photos';

  constructor(
    private apiServicePhotos: ApiService,
    private http: HttpClient
  ) { }

  getPhotos(): Observable<IPhoto[]> {
    this.setFullEndPoint();

    return this.http.get<IPhoto[]>(this.apiServicePhotos.fullApiEndPoint)
      .pipe(
        catchError(this.handleError)
      )
  }

  private handleError(err: HttpErrorResponse){
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = `An error occurred: ${err.error.message}`;
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  
  private setFullEndPoint(completeUrl: string = ''): void {
    this.apiServicePhotos.fullApiEndPoint = this.apiServicePhotos.apiUrl + 
      this.photosEndPoint +
      completeUrl;
  }
}
