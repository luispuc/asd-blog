import { Pipe, PipeTransform } from '@angular/core';
import { IConfiguration } from '../configuration';

@Pipe({
  name: 'filterNavList'
})
export class FilterNavListPipe implements PipeTransform {

  transform(navListElements: IConfiguration[], menuId: number): any {
    let newNavListElements: IConfiguration[] = []

    navListElements.forEach(element => {
      if( (menuId != element.menuId ) && (element.menuParentId == menuId) ){
        newNavListElements.push(element);
      }
    });

    return newNavListElements;
  }

}
