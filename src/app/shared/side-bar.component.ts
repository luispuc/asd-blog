import { Component, Input, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { IConfiguration } from '../configuration';
import { CustomNavListComponent } from './custom-nav-list.component';

@Component({
  selector: 'asd-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnDestroy {

  @ViewChild('snav', {static: true}) snav: MatSidenav;
  
  /**Se considera la siguiente estructura para los elementos
   * {
   *  "name": "string",
   *  "moduleId": 0,
   *  "menuParentId": 0,
   *  "permissionId": 0,
   *  "path": "string",
   *  "description": "string",
   *  "step": 0,
   *  "isInactive": true,
   *  "menuId": 0
   * }
   */
  mobileQuery: MediaQueryList;
  public show: boolean = false;

  @Input() menuElements: IConfiguration[] = [];

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  public toggle(): void{
    this.snav.toggle();

    this.show = !this.show;
  }

  private _mobileQueryListener: () => void;

}
