import { Component, OnInit, Input } from '@angular/core';
import { IConfiguration } from '../configuration';

@Component({
  selector: 'asd-custom-nav-list',
  templateUrl: './custom-nav-list.component.html',
  styleUrls: ['./custom-nav-list.component.css']
})
export class CustomNavListComponent implements OnInit {

  @Input() navListElements: IConfiguration[] = [];
  icon_right: string = 'keyboard_arrow_right';

  constructor() { }

  ngOnInit() {
  }

}
