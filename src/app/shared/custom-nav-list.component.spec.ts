import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomNavListComponent } from './custom-nav-list.component';

describe('CustomNavListComponent', () => {
  let component: CustomNavListComponent;
  let fixture: ComponentFixture<CustomNavListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomNavListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomNavListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
