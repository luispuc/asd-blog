import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { FormsModule } from '@angular/forms';
import { SideBarComponent } from './side-bar.component';
import { CustomNavListComponent } from './custom-nav-list.component';
import { FilterNavListPipe } from './filter-nav-list.pipe';

@NgModule({
  declarations: [SideBarComponent, CustomNavListComponent, FilterNavListPipe],
  imports: [
    MaterialModule
  ],
  exports: [
    MaterialModule,
    FormsModule,
    SideBarComponent
  ]
})
export class SharedModule { }
