import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from './api.service';
import { IConfiguration } from './configuration';
import { SideBarComponent } from './shared/side-bar.component';

@Component({
  selector: 'asd-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'ASD Blog';
  menuElements: IConfiguration[] = [];
  @ViewChild('snav', {static: true}) snav: SideBarComponent; 

  constructor(private apiService: ApiService){
    if(!localStorage.getItem('asd_token')){
      /*this.apiService.getToken().subscribe({
        next: data => localStorage.setItem('asd_token', JSON.stringify(data))
      });*/
      localStorage.setItem('asd_token', '{'+
        '"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3VzZXJkYXRhIjoie1wiVXNlcm5hbWVcIjpcImx1aXMucHVjQGFzZC10ZWNoLm14XCIsXCJVVUlEXCI6XCI4ZmJkYTQwZS1hY2Q4LWU5MTEtODE1NC1kNGFlNTJjZjk5ZThcIixcIlN5c1VzZXJJZFwiOjU2LFwiSXNUZW1wb3JhcnlQYXNzd29yZFwiOmZhbHNlfSIsImV4cCI6MTU3NzYwNTkyMSwiaXNzIjoiYXNkLXRlY2gubXgiLCJhdWQiOiJhc2QtdGVjaC5teCJ9.36SHqSvJ41BrzgO6geK-AExGJBDYnfC2rb77Aicb0K4",' +
        '"isTemporaryPassword": false'+
      '}')
    }
  }

  ngOnInit(){
    let tokenObj = JSON.parse(localStorage.getItem('asd_token'));
    let token = tokenObj.token;
    this.apiService.getConfiguration(token).subscribe({
      next: menus => this.menuElements = menus
    })
  }

  snavToggle(): void{
    this.snav.toggle();
  }
}
