import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Post } from './post';
import { UserService } from '../users/user.service';
import { IUser } from '../users/user';
import { NgForm } from '@angular/forms';

@Component({
  templateUrl: './dialog-add-post.component.html',
  styleUrls: ['./dialog-add-post.component.css']
})
export class DialogAddPostComponent implements OnInit {
  titleDialog: string = 'Add Post';
  users: IUser[];
  
  constructor(
    public dialogRef: MatDialogRef<DialogAddPostComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data: Post,
    private userServie: UserService
  ) {  
    //Retrieve the users data
    this.userServie.getUsers().subscribe({
      next: users => this.users = users,
      error: err => console.error(err)
    });
  }

  ngOnInit() {
    
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onSubmit(postForm: NgForm) {
    if(!postForm.valid)
      return false;

    this.dialogRef.close(this.data);
  }

}
