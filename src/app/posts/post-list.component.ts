import { Component, OnInit, ViewChild } from '@angular/core';
import { PostService } from './post.service';
import { IPost } from './post';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogAddPostComponent } from './dialog-add-post.component';
import { DialogDeleteConfirmComponent } from './dialog-delete-confirm.component';
import { IUser } from '../users/user';
import { UserService } from '../users/user.service';
import * as XLSX from 'xlsx';

const ADD: string = 'ADD';
const EDIT: string = 'EDIT';

@Component({
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  
  titleList: string = 'Posts';
  displayedColumns: string[] = ['id', 'user', 'title', 'body', 'actions'];
  loading: boolean = true;
  dataSource: MatTableDataSource<IPost>;
  users: IUser[];
  posts: IPost[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  errorMessage: string = '';

  constructor(
    private userService: UserService,
    private postService: PostService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.initData();
  }

  initData(){
    //show loading
    this.loading = true;
    this.userService.getUsers().subscribe({
      next: users => {
        this.users = users;

        this.getPosts();
      }
    });
  }

  getPosts(): void {
    this.postService.getPosts().subscribe({
      next: posts => {
        posts.forEach((post, i) => {
          posts[i].user = this.users.find(user => user.id == post.userId)
        });

        this.posts = posts;
        this.dataSource = new MatTableDataSource(this.posts);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        //hide loading
        this.loading = false;
      },
      error: err => this.errorMessage = err
    });
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(): void {
    const dialogAddPostRef = this.dialog.open(DialogAddPostComponent, {
      width: '500px',
      data: {}
    });

    dialogAddPostRef.afterClosed().subscribe(result => {
      if(!result)
        return false;
      
      this.postService.savePost(result).subscribe({
        next: (post: IPost) => this.updateDataTable(post, ADD),
        error: err => this.errorMessage = err
      })
    });
  }

  delete(id: number): void {
    const dialogDeletePost = this.dialog.open(DialogDeleteConfirmComponent, {
      data: {id:id}
    });

    dialogDeletePost.afterClosed().subscribe(result => {
      if(result){
        this.postService.deletePost(id).subscribe({
          next: result => {
            if(result){
              let indexPost = this.posts.indexOf((this.posts.find(post => post.id == id)));
    
              if(indexPost >= 0){
                this.posts.splice(indexPost, 1);
                this.updateDataTable();
              }
            }
          }
        });
      }
    })
  }

  edit(id: number): void {
    let post: IPost = this.dataSource.data.find(p => p.id == id);

    const dialogEditRef = this.dialog.open(DialogAddPostComponent, {
      width: '500px',
      data: post
    });

    dialogEditRef.afterClosed().subscribe(result => {
      if(!result)
        return false;

      this.postService.editPost(result).subscribe({
        next: (post: IPost) => this.updateDataTable(post, EDIT),
        error: err => this.errorMessage = err
      })
    });
  }

  downloadJSON(): void {
    //declare var json to print
    let json: Array<Object> = this.dataSourceToPrint();
    //make blob from json
    let blobJSON = new Blob(['\ufeff' + JSON.stringify(json, null,'\t')], {
      type: 'application/json;charset=utf-8'
    });
    //make url data
    let urlData = URL.createObjectURL(blobJSON);

    //send to download file
    this.downloadFile(urlData, '.json');
  }

  downloadExcel(): void {
    //parse data source
    let json: Array<Object> = this.dataSourceToPrint();
    //with xlsx library write the worksheet
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    //write the workbook
    const workbook: XLSX.WorkBook = {
      Sheets : {data: worksheet},
      SheetNames: ['Posts']
    };
    //generate excel buffer
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'array'
    });

    //make blob from excelBuffer 
    let blobExcel = new Blob([excelBuffer], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
    });
    //make url data from blob excel
    let urlData = URL.createObjectURL(blobExcel);

    this.downloadFile(urlData, '.xlsx');
  }

  refresh(): void {
    this.initData();
  }

  private updateDataTable(post?: IPost, action?: string): void {
    switch(action){
      case ADD:
        //So add the new post to data
        this.posts.push(post);
        break;
      case EDIT:
        //So replace the post edited
        this.posts.find((p, i) => {
          if(p.id == post.id){
            this.posts[i] = post
          }
        });
        break;
    }

    this.dataSource.data = this.posts;
  }

  private dataSourceToPrint(): Array<object> {
    let json: Array<Object> = []; 

    this.dataSource.data.forEach(post => {
      let jsonObj: Object = {
        id: post.id,
        userName: post.user.name,
        title: post.title,
        body: post.body
      };
      json.push(jsonObj);
    });

    return json;
  }

  private downloadFile(urlData: string, extesion: string, fileName: string = 'export_' + new Date().getTime()): void {
    //make element "a" to download file
    let a = document.createElement('a');
    //set target
    a.setAttribute('target', '_blank');
    a.setAttribute('href', urlData);
    
    let fileNameComplete = fileName + extesion;
    a.setAttribute('download', fileNameComplete);
    a.style.visibility = 'hidden';
    //Apply the element temporally to body and execute click event
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

}
