import { IUser } from '../users/user';

export interface IPost {
    userId: number;
    id: number;
    title: string;
    body: string;
    user: IUser;
}

export class Post {
    constructor(
        public id: number,
        public userId: number,
        public title: string,
        public body: string
    ){}
}
