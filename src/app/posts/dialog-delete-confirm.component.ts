import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    template: 
        `<h1 mat-dialog-title>{{titleDialog}}</h1>
        <div mat-dialog-content >
            <p>¿Está seguro de eliminar?, esta acción no podría deshacerse</p>
        </div>
        <div mat-dialog-actions align="end">
            <button mat-button (click)="onNoClick()">No</button>
            <button mat-button [mat-dialog-close]="true">Yes</button>
        </div>`
})

export class DialogDeleteConfirmComponent {
    titleDialog: string = 'Delete Post';

    constructor(public dialogRef: MatDialogRef<DialogDeleteConfirmComponent>,
                @Inject(MAT_DIALOG_DATA)
                public data: {}){}

    onNoClick(){
        this.dialogRef.close();
    }
}