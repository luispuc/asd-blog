import { NgModule } from '@angular/core';
import { PostListComponent } from './post-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DialogAddPostComponent } from './dialog-add-post.component';
import { DialogDeleteConfirmComponent } from './dialog-delete-confirm.component';

const routes: Routes = [
  {path: 'posts', component: PostListComponent}
];

@NgModule({
  declarations: [
    PostListComponent, 
    DialogAddPostComponent, 
    DialogDeleteConfirmComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  exports: [
  ],
  entryComponents: [
    DialogAddPostComponent,
    DialogDeleteConfirmComponent
  ]
})
export class PostModule { }
