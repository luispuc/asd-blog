import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddPostComponent } from './dialog-add-post.component';

describe('DialogAddPostComponent', () => {
  let component: DialogAddPostComponent;
  let fixture: ComponentFixture<DialogAddPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
