import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError} from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';

import { IPost, Post } from './post';
import { UserService } from '../users/user.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private postsEndPoint: string = '/posts';

  constructor(private apiServicePosts: ApiService, 
              private http: HttpClient,
              private userService: UserService) {
    
  }

  getPosts():Observable<IPost[]>{
    this.setFullEndPoint();

    return this.http.get<IPost[]>(this.apiServicePosts.fullApiEndPoint)
      .pipe(
        //tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
      )
  }

  getPost(id: number): Observable<IPost>{
    return this.getPosts().pipe(
      map((posts: IPost[]) => posts.find(p => p.id == id))
    )
  }

  savePost(post: Post): Observable<IPost>{
    this.setFullEndPoint();

    return this.http.post<IPost>(
      this.apiServicePosts.fullApiEndPoint, post)
      .pipe(
        map((post: IPost) => {
          let newPost: IPost = post;
          this.userService.getUser(newPost.userId).subscribe({
            next: user => newPost.user = user
          });
          return newPost;
        }),
        catchError(this.handleError)
      )
  }

  editPost(post: Post): Observable<IPost>{
    let completeUrl = '/' + post.id.toString();
    this.setFullEndPoint(completeUrl);

    return this.http.put(
      this.apiServicePosts.fullApiEndPoint, post)
      .pipe(
        map((post: IPost) => {
          let newPost = post;
          this.userService.getUser(newPost.userId).subscribe({
            next: user => newPost.user = user
          });
          return newPost;
        }),
        catchError(this.handleError)
      )
  }

  deletePost(id: number): Observable<boolean> {
    let completeUrl = '/' + id.toString();
    this.setFullEndPoint(completeUrl);
    
    return this.http.delete(this.apiServicePosts.fullApiEndPoint)
    .pipe(
      map(() => true),
      catchError(this.handleError)
    )
  }

  private handleError(err: HttpErrorResponse){
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        errorMessage = `An error occurred: ${err.error.message}`;
    } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  private setFullEndPoint(completeUrl: string = ''): void {
    this.apiServicePosts.fullApiEndPoint = this.apiServicePosts.apiUrl + 
      this.postsEndPoint +
      completeUrl;
  }
}
