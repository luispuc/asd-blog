import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { HttpCacheService } from './http-cache.service';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

const GET_METHOD = 'GET';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {

    constructor(private cacheService: HttpCacheService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //Request methods not intercepted
        if( req.method !== GET_METHOD ){
            console.log(`Invalidating cache: ${req.method} ${req.url}`);
            this.cacheService.invalidateCache();
            return next.handle(req);
        }

        //Retrieve response in cache
        const cachedResponse: HttpResponse<any> = this.cacheService.get(req.url);
        
        if(cachedResponse){
            console.log(`Returning a cached response: ${cachedResponse.url}`);  
            return of(cachedResponse);
        }

        //Send request to server and add response to cache
        return next.handle(req)
            .pipe(
                tap(event => {  
                    if (event instanceof HttpResponse) {  
                      console.log(`Adding item to cache: ${req.url}`);  
                      this.cacheService.put(req.url, event);  
                    }  
                })
            );
            
    }
}