import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { PostModule } from './posts/post.module';
import { HomeModule } from './home/home.module';
import { SharedModule } from './shared/shared.module';
import { AlbumModule } from './albums/album.module';
import { CacheInterceptor } from './cache.interceptor';
import { CatalogueModule } from './catalogues/catalogue.module';

const routes: Routes = [
  {path:'', redirectTo: 'home', pathMatch: 'full'},
  {path:'**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,//Never never forget import that module!
    RouterModule.forRoot(routes),
    SharedModule,
    HomeModule,
    PostModule,
    AlbumModule,
    CatalogueModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true}
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
